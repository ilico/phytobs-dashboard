import logging
import os.path
import pandas as pd
import plotly.colors as colors

survey_taxons_grp_abbrev = {}
survey_taxons = {}
survey_taxons_grp = {}
analyst_df = None
phytobs_df = None
hydro_df = None
hydro_units = {
    'Temperature': 'Deg. °C',
    'Salinity': 'Dmnless',
    'Oxygen': 'SOMLIT=mL/L,REPHY=mg/L',
    'CHLA': 'ug/l',
    'NH4': 'umol/l',
    'PO4': 'umol/l',
    'SIOH4': 'umol/l',
    'NO2_NO3': 'umol/l',
}
analyst_survey_info = {}
analyst_survey_csv = {}

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

def load_data(config):
    """
    Load data from specified URLs or cached files.

    :param config: Configuration dictionary containing URLs and file paths for data.
    :type config: dict

    :return: Tuple containing the loaded Analyst and PHYTOBS dataframes.
    :rtype: tuple
    """
    ANALYST_URL = config['analyst']['url']
    ANALYST_SEP = config['analyst']['separator']
    CACHED_ANALYST_FILENAME = config['analyst']['file']
    CACHED_HYDRO_FILENAME = config['hydro']['file']

    PHYTOBS_URL = config['phytobs']['url']
    PHYTOBS_SEP = config['phytobs']['separator']
    CACHED_PHYTOBS_FILENAME = config['phytobs']['file']

    analyst_file_abspath = os.path.abspath(CACHED_ANALYST_FILENAME)
    analyst_file_dirname = os.path.dirname(analyst_file_abspath)
    if not os.path.exists(analyst_file_dirname):
        os.makedirs(analyst_file_dirname)

    phytobs_file_abspath = os.path.abspath(CACHED_PHYTOBS_FILENAME)
    phytobs_file_dirname = os.path.dirname(phytobs_file_abspath)
    if not os.path.exists(phytobs_file_dirname):
        os.makedirs(phytobs_file_dirname)

    if os.path.exists(CACHED_ANALYST_FILENAME):
        LOGGER.debug("Loading analyst data from local file.")
        a_df = pd.read_pickle(CACHED_ANALYST_FILENAME)
        h_df = pd.read_pickle(CACHED_HYDRO_FILENAME)
    else:
        LOGGER.debug("Loading analyst data from URL.")
        full_a_df = pd.read_csv(ANALYST_URL,
                                sep=ANALYST_SEP,
                                low_memory=False)
        a_df = full_a_df.filter(
            [
                'site',
                'sampling_date',
                'longitude',
                'latitude',
                'min_depth',
                'max_depth',
                'aphia_id',
                'user_entered_taxon',
                'scientific_name',
                'count_result (cells/l)',
            ]).copy()

        a_df = a_df.rename(
            columns={
                'site': 'Survey',
                'sampling_date': 'Sampling date',
                'longitude': 'Longitude',
                'latitude': 'Latitude',
                'scientific_name': 'Accepted taxon name',
                'count_result (cells/l)': 'Count',
            }
        )

        a_df['Coordinates'] = list(zip(a_df['Latitude'], a_df['Longitude']))
        a_df['Accepted taxon name'] = a_df['Accepted taxon name'].apply(str)

        # Create additional columns used for aggregated bar charts.
        a_df["Sampling datetime"] = pd.to_datetime(a_df['Sampling date'])
        a_df['Year'] = a_df['Sampling datetime'].dt.year
        a_df['Month'] = a_df['Sampling datetime'].dt.month_name()
        a_df['Year month'] = (a_df['Sampling datetime'].dt.year).astype(str) + '-' + (
            a_df['Sampling datetime']).dt.strftime("%m")
        a_df['Date offset'] = (a_df['Sampling datetime'].dt.month * 100 + a_df['Sampling datetime'].dt.day - 320) % 1300
        a_df['Season'] = pd.cut(a_df['Date offset'], [0, 300, 602, 900, 1300],
                                labels=['Spring', 'Summer', 'Autumn', 'Winter'])



        h_df = full_a_df.filter(
            [
                'site',
                'sampling_date',
                'temperature (degC)',
                'salinity (Dmnless)',
                'oxygen (SOMLIT=mL/L,REPHY=mg/L)',
                'chla (ug/l)',
                'nh4 (umol/l)',
                'po4 (umol/l)',
                'sioh4 (umol/l)',
                'no2_no3 (umol/l)',
        ]).copy()

        h_df = h_df.rename(
            columns={
                'site': 'Survey',
                'sampling_date': 'Sampling date',
                'temperature (degC)': 'Temperature',
                'salinity (Dmnless)': 'Salinity',
                'oxygen (SOMLIT=mL/L,REPHY=mg/L)': 'Oxygen',
                'chla (ug/l)': 'CHLA',
                'nh4 (umol/l)': 'NH4',
                'po4 (umol/l)': 'PO4',
                'sioh4 (umol/l)': 'SIOH4',
                'no2_no3 (umol/l)': 'NO2_NO3',
            }
        )

        LOGGER.debug(f"Number of entries in complete h_df {h_df.count()}")
        h_df = h_df.drop_duplicates()
#        h_df = pd.unique(h_df[['Survey','Sampling date','Temperature','Salinity','Oxygen','CHLA','NH4','PO4','SIOH4','NO2_NO3']].values.ravel())
        LOGGER.debug(f"Number of unique entries in complete h_df {h_df.count()}")


        h_df["Sampling datetime"] = pd.to_datetime(h_df['Sampling date'])
        h_df['Year'] = h_df['Sampling datetime'].dt.year
        h_df['Month'] = h_df['Sampling datetime'].dt.month_name()
        h_df['Year month'] = (h_df['Sampling datetime'].dt.year).astype(str) + '-' + (
            h_df['Sampling datetime']).dt.strftime("%m")
        h_df['Date offset'] = (h_df['Sampling datetime'].dt.month * 100 + h_df['Sampling datetime'].dt.day - 320) % 1300
        h_df['Season'] = pd.cut(h_df['Date offset'], [0, 300, 602, 900, 1300],
                                labels=['Spring', 'Summer', 'Autumn', 'Winter'])

        a_df.to_pickle(CACHED_ANALYST_FILENAME)
        h_df.to_pickle(CACHED_HYDRO_FILENAME)

    if os.path.exists(CACHED_PHYTOBS_FILENAME):
        LOGGER.debug("Loading phytobs data from local file.")
        p_df = pd.read_pickle(CACHED_PHYTOBS_FILENAME)
    else:
        LOGGER.debug("Loading phytobs data from URL.")
        full_p_df = pd.read_csv(PHYTOBS_URL,
                                sep=PHYTOBS_SEP,
                                low_memory=False)
        p_df = full_p_df.filter(
            [
                'site',
                'sampling_date',
                'longitude',
                'latitude',
                'min_depth',
                'max_depth',
                'taxonomic_group',
                'count_result (cells/l)',
            ])

        p_df = p_df.rename(
            columns={
                'site': 'Survey',
                'sampling_date': 'Sampling date',
                'longitude': 'Longitude',
                'latitude': 'Latitude',
                'taxonomic_group': 'Taxonomic group',
                'count_result (cells/l)': 'Count',
            }
        )

        p_df['Coordinates'] = list(zip(p_df['Latitude'], p_df['Longitude']))
        p_df['Taxonomic group'] = p_df['Taxonomic group'].apply(str)

        # Create additional columns used for aggregated bar charts.
        p_df["Sampling datetime"] = pd.to_datetime(p_df['Sampling date'])
        p_df['Year'] = p_df['Sampling datetime'].dt.year
        p_df['Month'] = p_df['Sampling datetime'].dt.month_name()
        p_df['Year month'] = (p_df['Sampling datetime'].dt.year).astype(str) + '-' + (
            p_df['Sampling datetime']).dt.strftime("%m")
        p_df['Date offset'] = (p_df['Sampling datetime'].dt.month * 100 + p_df[
            'Sampling datetime'].dt.day - 320) % 1300
        p_df['Season'] = pd.cut(p_df['Date offset'], [0, 300, 602, 900, 1300],
                                labels=['Spring', 'Summer', 'Autumn', 'Winter'])

        p_df.to_pickle(CACHED_PHYTOBS_FILENAME)

    return (a_df, p_df, h_df)


def compute_most_abundant_taxa():
    """
    Compute the most abundant taxons for each survey.

    This function calculates the most abundant taxons for each survey based on the 'Count' column in the
    'macrofauna_df' dataframe. It groups the data by 'Survey', calculates the mean count for each taxon,
    selects the top 5 taxons, and creates an 'Others' category to hold the mean count of all other taxons.

    The computed results are stored in the 'survey_taxons' dictionary.

    :return:
    """
    LOGGER.info("Starting topmost taxon computation.")
    for survey in analyst_df['Survey'].unique():
        survey_data = analyst_df[analyst_df['Survey'] == survey]
        top_taxons = survey_data.groupby('Accepted taxon name')['Count'].sum().nlargest(5)
        others_count = survey_data[~survey_data['Accepted taxon name'].isin(top_taxons.index)]['Count'].mean()
        top_taxons = top_taxons.reset_index()
        top_taxons.loc[len(top_taxons)] = ['Others', others_count]
        # Générer des couleurs pour les taxons
        num_taxons = len(top_taxons)
        colorscale = colors.qualitative.Dark24[:num_taxons]
        top_taxons['Color'] = colorscale
        survey_taxons[survey] = top_taxons
    LOGGER.info("Done topmost taxon computation.")


def compute_most_abundant_taxon_group():
    LOGGER.info("Starting topmost taxon group computation.")
    for survey in phytobs_df['Survey'].unique():
        survey_data = phytobs_df[phytobs_df['Survey'] == survey]
        top_taxon_groups = survey_data.groupby('Taxonomic group')['Count'].sum().nlargest(5)
        others_count = survey_data[~survey_data['Taxonomic group'].isin(top_taxon_groups.index)]['Count'].sum()
        top_taxon_groups = top_taxon_groups.reset_index()
        top_taxon_groups.loc[len(top_taxon_groups)] = ['Others', others_count]
        # Generate colors for the taxonomic groups
        num_taxon_groups = len(top_taxon_groups)
        colorscale = colors.qualitative.Dark24[:num_taxon_groups]
        top_taxon_groups['Color'] = colorscale
        survey_taxons_grp[survey] = top_taxon_groups
    LOGGER.info("Done topmost taxon group computation.")

def compute_survey_info():
    LOGGER.info("Starting survey info computation.")

    survey_taxon_groups = []
    for survey in phytobs_df['Survey'].unique():
        survey_data = phytobs_df[phytobs_df['Survey'] == survey]
        lat, lon = survey_data.iloc[0]['Coordinates']
        min_date = survey_data['Sampling date'].min()
        max_date = survey_data['Sampling date'].max()
        nsamples = len(survey_data)
        analyst_survey_info[survey] = { 'lat' :  lat, 'lon' : lon, 'min_date' : min_date, 'max_date' : max_date, 'nsamples' : nsamples}
        analyst_survey_csv[survey] = survey_data.to_csv
        survey_taxon_groups += list(survey_data['Taxonomic group'].unique())

    max_abbrev_len = 25
    for taxon_group in survey_taxon_groups :
        abbrev_len=min(max_abbrev_len,len(taxon_group))
        abbrev_taxon_group = taxon_group[0:abbrev_len]
        if len(taxon_group) > max_abbrev_len :
            abbrev_taxon_group += ' (...)'
        survey_taxons_grp_abbrev[taxon_group]=abbrev_taxon_group

    LOGGER.info("Done survey info computation.")
