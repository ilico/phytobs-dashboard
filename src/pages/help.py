import dash
from dash import html, dcc

dash.register_page(__name__, order=3)

layout = html.Div(
    dcc.Markdown('''
#### Using the PHYTOBS Data Viewer

##### A word about the nature of the data

It is important to remember that the PHYTOBS Data Viewer provides access to two different types of microphytoplankton abundance data:

1. **Analyst data** giving the counts of individual taxa as they are determined by the analyst carrying out the counting process.

2. **PHYTOBS data** giving the counts of normalized taxa or taxon groups described in a reference table. The taxa or
taxon groups in the reference table are those that are agreed upon by the community of PHYTOBS data producers. For taxon groups
the counts are the sum of the counts of all individual taxa included in the group. Similarly, in the reference table, taxa
may be located at the genus (or higher) taxonomic level. In that case, the associated counts are the sum of all *analyst counts*
associated to a taxon belonging to the higher-level taxon.

Details on the data acquisition protocol can be found on the [PHYTOBS Web site](https://www.phytobs.fr).

##### The Data Viewer Screen Layout

The Data Viewer screen is made of three distinct areas:

1. **The Map and Summary Area**

![Screenshot of the Map and Summary Area](assets/images/screenshots/map_summary_area.png)

Located at the top of the screen, this area shows a map where each PHYTOBS sampling station is represented by a marker.
Clicking on a sampling station marker displays a summary of the data available for that station. Moreover, a CSV file with
actual taxon group counts can be downloaded using the provided button.

Below the map, and not shown by default, two summary tables can be displayed clicking on the triangle left to *PHYTOBS SNO Data Summary Tables*. These
tables describe what data is available both for *Analyst* and *PHYTOBS* counts.

2. **The Graph Control Area**

![Screenshot of the Graph Control Area](assets/images/screenshots/graph_control_area.png)

The Graph Control area, located on the left hand side of the screen, serves two main purposes: add or remove graphs to or from the *Graph Display Area*; and define the
parameters of the currently active graph (which depend on the type of the active graph).

3. **The Graph Display Area**

![Screenshot of the Graph Display](assets/images/screenshots/graph_display_area.png)

The graph display area occupies the major part of the screen. This is were all the graphical representations are generated when they are added with the graph controls.

##### Using the Graph Controls

1. Adding a Graph

At the top of the Graph Control Area, a series of green and blue buttons allow the addition of a new graph to the Graph Display Area. Each button matches a specific graph type :

- *Analyst*: creates a new graph with Analyst occurrence counts.
- *PHYTOBS*: creates a new graph with PHYTOBS SNO Taxon and Taxon Group occurrence counts.
- *HYDRO*: creates a new graph with hydrological parameters (courtesy of the SOMLIT SNO).
- *Top Analyst*: creates a new graph with the most frequent taxons based on Analyst occurrence counts.
- *Top PHYTOBS*: creates a new graph with the most frequent PHYTOBS SNO Taxon or Taxon Group occurrence counts.

Creating a new graph always makes the newly created graph the active graph. A new tab will be generated below the graph addition buttons with a predefined
title depending on the graph type, such as "TP-Graph 1" for the first *Top PHYTOBS* graph, or "A-Graph 2" for a newly created *Analyst* graph. 

Parameters for a newly created grapg are set randomly, and can then be directly adjusted using
the active graph controls which make out the main part of the Graph Control Area. 

2. Defining Graph Parameters

Each graph type comes with its own set of parameters which can be individually tuned. Adjusting a parameter will refresh the matchin graph in
the graph display area. Some graphs may take some time to refresh, depending on the amount of data that needs to be processed. The most time-consuming
operations are those needed to compute the *Top Taxon* or *Top Taxon Group* graphs, especially when setting the *Top N Taxa or Taxon Group* to high values. 

3. Switching Between Active Graphs

Any graph can be made active by clicking on the tab with its name. This gives access to the graphs parameters.

4. Duplicating Graphs

In order to allow easy comparisons between graphs, it is possible to duplicate the currently active graph using the *Duplicate* button
in the lower right corner of the *Graph Control Area*. This generates a new graph with the same settings as the active graphs, which makes it
then easy to change a single or a few parameters and visually compare the graphs in the *Graph Display Area*.

5. Removing Graphs

The currently active graph can be removed using the *Remove* button next to the *Duplicate* button. If only one graph remains, it cannot be removed.

##### Moving Graphs Up and Down

In the *Graph Display Area*, graphs are generated from top to bottom as they are created. To vertically reorganize the order of the graphs, two arrows are provided below
each graph to move it up and down in the *Graph Display Area*.


    '''), className="bd-content-full")