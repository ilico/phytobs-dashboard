import logging

import dash
import pandas as pd
from dash import Dash, html, dcc, callback, Output, Input, ALL, MATCH, State, Patch, ctx
import dash_ag_grid as dag
import plotly.figure_factory as ff

import phytobs_data_tools as pdt

dash.register_page(__name__,order=1)

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


def analyst_table_info():
    """
    Create and return a data table displaying analyst information.

    :return: The Dash Ag-Grid table for analyst information.
    :rtype: dag.AgGrid
    """
    # Calcul des statistiques pour chaque étude
    df_analyst_table_info = pdt.analyst_df.groupby('Survey').agg({'Sampling date': ['nunique', 'min', 'max'],
                                                                  'Accepted taxon name': ['count', 'nunique'],
                                                                  'Latitude': 'first', 'Longitude': 'first'})
    df_analyst_table_info.columns = ['Samples', 'First sample', 'Latest sample', 'Total occurrences',
                                     'Distinct Taxa', 'Latitude', 'Longitude']

    df_analyst_table_info.reset_index(inplace=True)

    # Création du tableau Ag-Grid
    analyst_table = dag.AgGrid(
        id='analyst-data-table',
        columnDefs=[{"field": col, "headerName": col} for col in df_analyst_table_info.columns],
        rowData=df_analyst_table_info.to_dict('records'),
        defaultColDef={"resizable": True, "sortable": True, "filter": True},
        columnSize="sizeToFit",
        csvExportParams={
            "fileName": "analyst_table.csv",
        },
    )

    return analyst_table


def phytobs_table_info():
    """
    Create and return a data table displaying phytobs information.

    :return: The Dash Ag-Grid table for phytobs information.
    :rtype: dag.AgGrid
    """
    df_phytobs_table_info = pdt.phytobs_df.groupby('Survey').agg({'Sampling date': ['nunique', 'min', 'max'],
                                                                  'Taxonomic group': ['count', 'nunique'],
                                                                  'Latitude': 'first', 'Longitude': 'first'})
    df_phytobs_table_info.columns = ['Samples', 'First sample', 'Latest sample', 'Total Grouped Occurrences',
                                     'Distinct Groups', 'Latitude', 'Longitude']

    df_phytobs_table_info.reset_index(inplace=True)

    # Création du tableau Ag-Grid
    phytobs_table = dag.AgGrid(
        id='phytobs-data-table',
        columnDefs=[{"field": col, "headerName": col} for col in df_phytobs_table_info.columns],
        rowData=df_phytobs_table_info.to_dict('records'),
        defaultColDef={"resizable": True, "sortable": True, "filter": True},
        columnSize="sizeToFit",
        csvExportParams={
            "fileName": "phytobs_table.csv",
        },
    )

    return phytobs_table


@callback(
    Output("analyst-data-table", "exportDataAsCsv"),
    Input("a-csv-button", "n_clicks"),
)
def export_analyst_data_as_csv(n_clicks):
    if n_clicks:
        return True
    return False


@callback(
    Output("phytobs-data-table", "exportDataAsCsv"),
    Input("p-csv-button", "n_clicks"),
)
def export_phytobs_data_as_csv(n_clicks):
    if n_clicks:
        return True
    return False



def summary_table_info():

    summary_info = pdt.phytobs_df[['Survey','Sampling date']]

    summary_info["Sampling date"] = pd.to_datetime(summary_info["Sampling date"])
    summary_info["Start"] = summary_info["Sampling date"].apply(lambda x: pd.offsets.MonthBegin().rollback(x))
    summary_info["Start"] = summary_info["Start"].apply(lambda x: x.replace(hour=0, minute=0, second=0))
    summary_info["Finish"] =summary_info['Sampling date'].apply(lambda x: pd.offsets.MonthEnd().rollforward(x))
    summary_info["Finish"] = summary_info["Finish"].apply(lambda x: x.replace(hour=0, minute=0, second=0))
    summary_info.drop('Sampling date', axis=1, inplace=True)
    summary_info=summary_info.drop_duplicates(keep='first')
    summary_info["Task"] = summary_info["Survey"]
    summary_info["Resource"] = "Samples"

    summary_info.sort_values(['Task'], inplace=True)

    summary_info.rename(columns={'Task': 'Task', "Start" : "Start", "Finish" : 'Finish'}, inplace=True)

    colors = {
        'Samples' : 'rgb(47,164,90)',
    }

    fig = ff.create_gantt(summary_info, colors=colors, index_col="Resource", show_colorbar=False, title="Overview of Sampling Timespan", group_tasks=True)

    return dcc.Graph(figure=fig)



layout = html.Div(
html.Div(
            children=[
                html.Div(
                    children=[
                        html.H4(f"Overview of Available Samples"),
                        summary_table_info(),
                    ]
                ),
                html.Div(
                    children=[
                        html.H4(f"PHYTOBS taxon groups table ({pdt.phytobs_df['Survey'].nunique()} surveys)"),
                        phytobs_table_info(),
                        html.Button("Download CSV", id="p-csv-button", n_clicks=0),
                    ]
                ),
                html.Div(
                    children=[
                        html.H4(f"PHYTOBS analyst taxon table ({pdt.analyst_df['Survey'].nunique()} surveys)"),
                        analyst_table_info(),
                        html.Button("Download CSV", id="a-csv-button", n_clicks=0),
                    ]
                ),
            ], className="bd-content-full"
        ),
)
