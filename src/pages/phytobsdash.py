import datetime
import calendar
import re
import random
import dash_leaflet as dl
import dash_ag_grid as dag
from dash import Dash, html, dcc, callback, Output, Input, ALL, MATCH, State, Patch, ctx
import dash
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import os.path
import logging

import phytobs_data_tools as pdt

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

controls_by_index = {}
taxon_layers_by_index = {}

dash.register_page(__name__, path="/", name="Home", order=0)


def _build_taxon_group_dropdown_options(survey_df):
    options = []
    survey_taxons = sorted(survey_df['Taxonomic group'].unique())
    for taxon in survey_taxons:
        abbrev = pdt.survey_taxons_grp_abbrev[taxon]
        options.append({'label': abbrev, 'value': taxon, 'title': taxon})
    return options



@callback(
    Output('survey-info-layer', 'children'),
    Output('analyst-taxon-layer', 'children'),
    Output('phytobs-taxon-layer', 'children'),
    Input('survey-info-layer', 'id'),
    Input('analyst-taxon-layer', 'id'),
    Input('phytobs-taxon-layer', 'id')
)
def update_layers(survey_info_layer, analyst_taxon_layer, phytobs_taxon_layer):
    """
    Update the layers of the map with survey information, taxon ans taxon group markers.

    :param survey_info_layer: The ID of the survey info layer.
    :type survey_info_layer: str
    :param analyst_taxon_layer: The ID of the analyst taxon layer.
    :type taxon_layer: str
    :param phytobs_taxon_layer: The ID of the phutobs taxon group layer.
    :type taxon_layer: str

    :return: A tuple containing the survey info markers, taxon markers and taxon group markers.
    :rtype: tuple
    """

    LOGGER.debug("Starting update_layers")
    survey_info_markers = []
    taxon_markers = []
    taxon_group_markers = []

    all_surveys = pdt.analyst_df['Survey'].unique()
    for survey in all_surveys:
        lat = pdt.analyst_survey_info[survey]['lat']
        lon = pdt.analyst_survey_info[survey]['lon']
        min_date = pdt.analyst_survey_info[survey]['min_date']
        max_date = pdt.analyst_survey_info[survey]['max_date']
        nsamples = pdt.analyst_survey_info[survey]['nsamples']

        survey_info = html.Div([
            html.H4(f"Survey: {survey}"),
            html.P(f"First Sampling Date: {min_date}"),
            html.P(f"Last Sampling Date: {max_date}"),
            html.P(f"Number of Sampling: {nsamples}"),
            html.P(f"Coordinates: {lat}, {lon}"),

            # html.A([
            #     html.Button('Download XLSX', id={'type' : 'btn-download-xlsx', 'index' : f"{survey}"}),
            #     dcc.Download(id={'type' : 'download-xlsx', 'index' : f"{survey}"})
            #     ]
            # ),
            html.A([
                html.Button('Download CSV', id={'type': 'btn-download-csv', 'index': f"{survey}"}),
                dcc.Download(id={'type': 'download-csv', 'index': f"{survey}"})
            ]
            ),
            # html.A([
            #     html.Button('Download JSON', id={'type' : 'btn-download-json', 'index' : f"{survey}"}),
            #     dcc.Download(id={'type' : 'download-json', 'index' : f"{survey}"})
            #     ]
            # ),
        ])

        survey_info_marker = dl.Marker(
            position=[lat, lon],
            children=dl.Popup([survey_info], closeOnClick=True),
            title=survey
        )
        survey_info_markers.append(survey_info_marker)

        taxon_names = pdt.survey_taxons[survey]['Accepted taxon name']
        counts = pdt.survey_taxons[survey]['Count']
        colors = pdt.survey_taxons[survey]['Color']

        fig = go.Figure(
            data=[
                go.Pie(
                    labels=taxon_names,
                    values=counts,
                    marker=dict(colors=colors))],
        )
        fig.update_layout(
            height=350,
            width=350,
            margin=dict(l=15, r=15, t=35, b=15),
            legend=dict(orientation='h', x=0.5, y=-0.2),
            title=f"Top 5 taxons in {survey}",
        )

        title = html.H4(f"A-Survey: {survey}")
        popup_content = html.Div([title, dcc.Graph(figure=fig, )],
                                 style={'width': 'auto', 'height': 'auto'})

        taxon_marker = dl.Marker(
            position=[lat, lon],
            children=dl.Popup([popup_content], closeOnClick=True)
        )
        taxon_markers.append(taxon_marker)

    all_surveys = pdt.phytobs_df['Survey'].unique()
    for survey in all_surveys:
        survey_data = pdt.phytobs_df[pdt.phytobs_df['Survey'] == survey]
        lat, lon = survey_data.iloc[0]['Coordinates']

        taxon_group_info = pdt.survey_taxons_grp.get(survey)
        if taxon_group_info is not None:
            taxon_group_names = taxon_group_info['Taxonomic group']
            counts = taxon_group_info['Count']
            colors = taxon_group_info['Color']

            fig_group = go.Figure(
                data=[
                    go.Pie(
                        labels=taxon_group_names,
                        values=counts,
                        marker=dict(colors=colors))],
            )
            fig_group.update_layout(
                height=350,
                width=350,
                margin=dict(l=15, r=15, t=35, b=15),
                legend=dict(orientation='h', x=0.5, y=-0.2),
                title=f"Top 5 taxon groups in {survey}",
            )

            title_group = html.H4(f"P-Survey: {survey}")
            popup_content_group = html.Div([title_group, dcc.Graph(figure=fig_group, )],
                                           style={'width': 'auto', 'height': 'auto'})

            taxon_group_marker = dl.Marker(
                position=[lat, lon],
                children=dl.Popup([popup_content_group], closeOnClick=True)
            )
            taxon_group_markers.append(taxon_group_marker)

    LOGGER.debug("Done update_layers")

    return survey_info_markers, taxon_markers, taxon_group_markers


@callback(
    Output({'type': 'download-csv', 'index': MATCH}, 'data'),
    Input({'type': 'btn-download-csv', 'index': MATCH}, 'n_clicks'),
    State({'type': 'btn-download-csv', 'index': MATCH}, 'id')
)
def download_csv_for_survey(n_clicks, id):
    survey = id['index']
    csv_data_frame = pdt.analyst_survey_csv[survey]
    return dcc.send_data_frame(csv_data_frame, f"phythobs-data-{survey}.csv")


@callback(
    Output({'type': 'download-xlsx', 'index': MATCH}, 'data'),
    Input({'type': 'btn-download-xlsx', 'index': MATCH}, 'n_clicks'),
    State({'type': 'btn-download-xlsx', 'index': MATCH}, 'id')
)
def download_xlsx_for_survey(n_clicks, id):
    survey = id['index']
    survey_data = pdt.analyst_df[pdt.analyst_df['Survey'] == survey]
    return dcc.send_data_frame(survey_data.to_excel, f"phythobs-data-{survey}.xlsx")


@callback(
    Output({'type': 'download-json', 'index': MATCH}, 'data'),
    Input({'type': 'btn-download-json', 'index': MATCH}, 'n_clicks'),
    State({'type': 'btn-download-json', 'index': MATCH}, 'id')
)
def download_json_for_survey(n_clicks, id):
    survey = id['index']
    survey_data = pdt.analyst_df[pdt.analyst_df['Survey'] == survey]
    return dcc.send_data_frame(survey_data.to_json, f"phythobs-data-{survey}.json")


@callback([
    Output('graph-controls', 'children'),
    Output('remove-graph-button', 'disabled'),
    Output('graph-controls', 'active_tab'),
],
    Input('add-analyst-graph-button', 'n_clicks'),
    Input('add-phytobs-graph-button', 'n_clicks'),
    Input('add-parameters-graph-button', 'n_clicks'),
    Input('add-top-taxon-graph-button', 'n_clicks'),
    Input('add-top-taxon-group-graph-button', 'n_clicks'),
    Input('remove-graph-button', 'n_clicks'),
    Input('duplicate-graph-button', 'n_clicks'),
    State('graph-controls', 'active_tab'),
    State('graph-controls', 'children'),
)
def add_remove_graph_controls(n_clicks_add_analyst, n_clicks_add_phytobs, n_clicks_add_parameters,
                              n_clicks_add_top_taxon,
                              n_clicks_add_top_taxon_group, n_clicks_remove, n_clicks_duplicate, active_tab_index,
                              children):
    LOGGER.debug("Starting add_remove_graph_controls")

    button_clicked = ctx.triggered_id

    if n_clicks_add_analyst is None:
        n_clicks_add_analyst = 0

    if n_clicks_add_phytobs is None:
        n_clicks_add_phytobs = 0

    if n_clicks_add_parameters is None:
        n_clicks_add_parameters = 0

    if n_clicks_add_top_taxon is None:
        n_clicks_add_top_taxon = 0

    if n_clicks_add_top_taxon_group is None:
        n_clicks_add_top_taxon_group = 0

    if n_clicks_duplicate is None:
        n_clicks_duplicate = 0

    next_index = n_clicks_add_analyst + n_clicks_add_phytobs + n_clicks_add_parameters + n_clicks_add_top_taxon + \
                 n_clicks_add_top_taxon_group + n_clicks_duplicate

    duplicate_type = None
    if button_clicked == 'duplicate-graph-button':
        active_tab_index_int = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index_int]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']
        duplicate_type = controls_by_index[f"index-{active_tab_id_index}"]['type']

    new_active_tab = f"tab-{len(children)}"

    if button_clicked == 'add-analyst-graph-button' or duplicate_type == 'analyst':
        (children, disabled) = add_remove_analyst_controls(next_index, active_tab_index, children, button_clicked)
        LOGGER.debug("Done add_remove_graph_controls")
        return children, disabled, new_active_tab

    if button_clicked == 'add-phytobs-graph-button' or duplicate_type == 'phytobs':
        (children, disabled) = add_remove_phytobs_controls(next_index, active_tab_index, children, button_clicked)
        LOGGER.debug("Done add_remove_graph_controls")
        return children, disabled, new_active_tab

    if button_clicked == 'add-parameters-graph-button' or duplicate_type == 'parameters':
        (children, disabled) = add_remove_hydro_controls(next_index, active_tab_index, children, button_clicked)
        LOGGER.debug("Done add_remove_graph_controls")
        return children, disabled, new_active_tab

    if button_clicked == 'add-top-taxon-graph-button' or duplicate_type == 'top-taxon':
        (children, disabled) = add_remove_top_taxon_controls(next_index, active_tab_index, children, button_clicked)
        LOGGER.debug("Done add_remove_graph_controls")
        return children, disabled, new_active_tab

    if button_clicked is None or button_clicked == 'add-top-taxon-group-graph-button' or duplicate_type == 'top-taxon-group':
        (children, disabled) = add_remove_top_taxon_group_controls(next_index, active_tab_index, children,
                                                                   button_clicked)
        LOGGER.debug("Done add_remove_graph_controls")
        return children, disabled, new_active_tab

    if button_clicked == 'remove-graph-button' and active_tab_index:
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        LOGGER.info(f"Remove Graph Controls - Active index: {active_tab_id}")
        patched_children = []
        disabled = True
        for child in children:
            child_id = child['props']['id']
            LOGGER.info(f"Child id {child_id}")
            if child_id['type'] == 'graph-control-tab' and child_id['index'] == active_tab_id['index']:
                continue
            patched_children.append(child)
        if len(patched_children) > 1:
            disabled = False
        if active_tab_index < len(patched_children):
            new_active_tab = f"tab-{active_tab_index}"
        else:
            new_active_tab = f"tab-{len(patched_children) - 1}"

        LOGGER.debug("Done add_remove_graph_controls")
        return patched_children, disabled, new_active_tab


def add_remove_analyst_controls(next_index, active_tab_index, children, button_clicked):
    LOGGER.debug("Starting add_remove_analyst_controls")

    disabled = True

    LOGGER.info(f"Generating Analyst controls at index {next_index}")

    new_control_values = {'type': 'analyst'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    survey_options = sorted(pdt.analyst_df['Survey'].unique())
    survey_value = survey_options[random.randrange(len(survey_options) - 1)]
    new_control_values['survey'] = survey_value

    survey_df = pdt.analyst_df[pdt.analyst_df['Survey'] == survey_value]
    taxon_options = sorted(survey_df['Accepted taxon name'].unique())
    taxon_value = [taxon_options[random.randrange(len(taxon_options) - 1)]]
    new_control_values['taxons'] = taxon_value

    start_date_value = min(pdt.analyst_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(pdt.analyst_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'sample'
    new_control_values['aggregation'] = aggregation_value

    if button_clicked == 'duplicate-graph-button':
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        survey_df = pdt.analyst_df[pdt.analyst_df['Survey'] == survey_value]
        taxon_options = sorted(survey_df['Accepted taxon name'].unique())
        taxon_value = controls_by_index[f"index-{active_tab_id_index}"]['taxons']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']

    controls_by_index[f"index-{next_index}"] = new_control_values

    survey_select = dcc.Dropdown(
        id={"type": "analyst-survey-dropdown", "index": next_index},
        options=survey_options,
        value=survey_value,
        style={'color': '#000'}

    )

    new_survey_dropdown = html.Div([
        dbc.Label("Survey"),
        survey_select
    ])

    taxon_dropdown = dcc.Dropdown(
        id={"type": "analyst-taxon-dropdown", "index": next_index},
        options=taxon_options,
        multi=True,
        value=taxon_value,
        style={'color': '#000'}
    )
    new_taxon_dropdown = html.Div([
        dbc.Label("Taxon :"),
        taxon_dropdown,
    ])

    new_date_picker = html.Div([
        dbc.Label('Date :'),
        dcc.DatePickerRange(
            id={"type": 'analyst-survey-date-picker', "index": next_index},
            min_date_allowed=min(pdt.analyst_df['Sampling date']),
            max_date_allowed=max(pdt.analyst_df['Sampling date']),
            initial_visible_month=max(pdt.analyst_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Graph Type :'),
        dcc.Dropdown(
            id={"type": 'analyst-graph-type', "index": next_index},
            options=[
                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Box plot', 'value': 'box'},
                {'label': 'Pie chart', 'value': 'pie'}
            ],
            value=graph_type_value,
            style={'color': '#000'}
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('Aggregation per :'),
        dcc.RadioItems(
            id={"type": 'analyst-survey-aggregation', "index": next_index},
            options=[
                {'label': 'Sample', 'value': 'sample'},
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'}

            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_survey_dropdown,
            new_taxon_dropdown,
            new_date_picker,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"Analyst-Graph {next_index + 1}",
    )

    patched_children.append(new_tab)
    LOGGER.debug("Done add_remove_analyst_controls")

    return patched_children, disabled


def add_remove_phytobs_controls(next_index, active_tab_index, children, button_clicked):
    LOGGER.debug("Starting add_remove_phytobs_controls")

    disabled = True

    LOGGER.info(f"Generating PHYTOBS controls at index {next_index}")

    new_control_values = {'type': 'phytobs'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    survey_options = sorted(pdt.phytobs_df['Survey'].unique())
    survey_value = survey_options[random.randrange(len(survey_options) - 1)]
    new_control_values['survey'] = survey_value

    survey_df = pdt.phytobs_df[pdt.phytobs_df['Survey'] == survey_value]
    taxon_options = _build_taxon_group_dropdown_options(survey_df)
    taxon_value = [taxon_options[random.randrange(len(taxon_options) - 1)]['value']]
    new_control_values['taxons'] = taxon_value

    start_date_value = min(pdt.phytobs_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(pdt.phytobs_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'sample'
    new_control_values['aggregation'] = aggregation_value

    if button_clicked == 'duplicate-graph-button':
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        survey_df = pdt.phytobs_df[pdt.phytobs_df['Survey'] == survey_value]
        taxon_options = _build_taxon_group_dropdown_options(survey_df)
        taxon_value = controls_by_index[f"index-{active_tab_id_index}"]['taxons']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']

    controls_by_index[f"index-{next_index}"] = new_control_values

    survey_select = dcc.Dropdown(
        id={"type": "phytobs-survey-dropdown", "index": next_index},
        options=survey_options,
        value=survey_value,
        style={'color': '#000'}

    )

    new_survey_dropdown = html.Div([
        dbc.Label("Survey"),
        survey_select
    ])

    taxon_dropdown = dcc.Dropdown(
        id={"type": "phytobs-taxon-dropdown", "index": next_index},
        options=taxon_options,
        multi=True,
        value=taxon_value,
        style={'color': '#000'}
    )
    new_taxon_dropdown = html.Div([
        dbc.Label("Taxon :"),
        taxon_dropdown,
    ])

    new_date_picker = html.Div([
        dbc.Label('Date :'),
        dcc.DatePickerRange(
            id={"type": 'phytobs-survey-date-picker', "index": next_index},
            min_date_allowed=min(pdt.phytobs_df['Sampling date']),
            max_date_allowed=max(pdt.phytobs_df['Sampling date']),
            initial_visible_month=max(pdt.phytobs_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Graph Type :'),
        dcc.Dropdown(
            id={"type": 'phytobs-graph-type', "index": next_index},
            options=[
                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Box plot', 'value': 'box'},
                {'label': 'Pie chart', 'value': 'pie'}
            ],
            value=graph_type_value,
            style={'color': '#000'}
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('Aggregation per :'),
        dcc.RadioItems(
            id={"type": 'phytobs-survey-aggregation', "index": next_index},
            options=[
                {'label': 'Sample', 'value': 'sample'},
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'}

            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_survey_dropdown,
            new_taxon_dropdown,
            new_date_picker,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"PHYTOBS-Graph {next_index + 1}",
    )

    patched_children.append(new_tab)

    LOGGER.debug("Done add_remove_phytobs_controls")
    return patched_children, disabled


def add_remove_top_taxon_controls(next_index, active_tab_index, children, button_clicked):
    """
    Generate controls for Top N Taxons graphs.

    :param next_index: The next index for the new tab.
    :param active_tab_index: The index of the active tab.
    :param children: List of children components in the layout.
    :param button_clicked: The identifier of the button that was clicked.
    :return: A tuple containing the updated children components and a disabled flag.
    """
    LOGGER.debug("Starting add_remove_top_taxon_controls")

    disabled = True

    LOGGER.info(f"Generating Top N Taxons controls at index {next_index}")

    new_control_values = {'type': 'top-taxon'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    survey_options = sorted(pdt.analyst_df['Survey'].unique())
    survey_value = survey_options[random.randrange(len(survey_options) - 1)]
    new_control_values['survey'] = survey_value

    survey_df = pdt.analyst_df[pdt.analyst_df['Survey'] == survey_value]
    max_top_taxons = len(survey_df['Accepted taxon name'].unique())
    top_taxons_value = min(10, max_top_taxons)
    new_control_values['top_taxon'] = top_taxons_value

    start_date_value = min(pdt.analyst_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(pdt.analyst_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'year'
    new_control_values['aggregation'] = aggregation_value

    computation_method_value = 'cumulative'
    new_control_values['computation_method'] = computation_method_value

    if button_clicked == 'duplicate-graph-button':
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        survey_df = pdt.analyst_df[pdt.analyst_df['Survey'] == survey_value]
        max_top_taxons = len(survey_df['Accepted taxon name'].unique())
        top_taxons_value = controls_by_index[f"index-{active_tab_id_index}"]['top_taxon']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        computation_method_value = controls_by_index[f"index-{active_tab_id_index}"]['computation_method']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']

    controls_by_index[f"index-{next_index}"] = new_control_values

    survey_select = dcc.Dropdown(
        id={"type": "top-taxon-survey-dropdown", "index": next_index},
        options=survey_options,
        value=survey_value)

    new_survey_dropdown = html.Div([
        dbc.Label("Survey"),
        survey_select
    ])

    new_top_taxon_count = html.Div([
        dbc.Label('Top N Taxa:'),
        dbc.Input(
            id={"type": 'top-taxon-input', "index": next_index},
            type='number',
            value=top_taxons_value,
            min=1,
            max=max_top_taxons
        ),
    ])

    new_date_picker = html.Div([
        dbc.Label('Date :'),
        dcc.DatePickerRange(
            id={"type": 'top-taxon-survey-date-picker', "index": next_index},
            min_date_allowed=min(pdt.analyst_df['Sampling date']),
            max_date_allowed=max(pdt.analyst_df['Sampling date']),
            initial_visible_month=max(pdt.analyst_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value,
        ),
    ])

    new_computation_method_choice = html.Div([
        dbc.Label('Computation Method :'),
        dcc.Dropdown(
            id={"type": 'top-taxon-computation-method', "index": next_index},
            options=[
                {'label': 'Average', 'value': 'average'},
                {'label': 'Cumulative', 'value': 'cumulative'},
            ],
            value=computation_method_value
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Graph Type :'),
        dcc.Dropdown(
            id={"type": 'top-taxon-graph-type', "index": next_index},
            options=[
                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Stacked histogram', 'value': 'stacked histogram'},
                {'label': 'Box plot', 'value': 'box'},
                {'label': 'Pie chart', 'value': 'pie'}
            ],
            value=graph_type_value
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('Aggregation per :'),
        dcc.RadioItems(
            id={"type": 'top-taxon-survey-aggregation', "index": next_index},
            options=[
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'}

            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_survey_dropdown,
            new_top_taxon_count,
            new_date_picker,
            new_computation_method_choice,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"Top Analyst Graph {next_index + 1}")

    patched_children.append(new_tab)
    LOGGER.debug("Done add_remove_top_taxon_controls")

    return patched_children, disabled


def add_remove_top_taxon_group_controls(next_index, active_tab_index, children, button_clicked):
    """
    Generate controls for Top N Taxons group graphs.

    :param next_index: The next index for the new tab.
    :param active_tab_index: The index of the active tab.
    :param children: List of children components in the layout.
    :param button_clicked: The identifier of the button that was clicked.
    :return: A tuple containing the updated children components and a disabled flag.
    """
    LOGGER.debug("Starting add_remove_top_taxon_group_controls")

    disabled = True

    LOGGER.info(f"Generating Top N Taxons Group controls at index {next_index}")

    new_control_values = {'type': 'top-taxon-group'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    survey_options = sorted(pdt.phytobs_df['Survey'].unique())
    survey_value = survey_options[random.randrange(len(survey_options) - 1)]
    new_control_values['survey'] = survey_value

    survey_df = pdt.phytobs_df[pdt.phytobs_df['Survey'] == survey_value]
    max_top_taxons = len(survey_df['Taxonomic group'].unique())
    top_taxons_value = min(10, max_top_taxons)
    new_control_values['top_taxon_group'] = top_taxons_value

    start_date_value = min(pdt.phytobs_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(pdt.phytobs_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'year'
    new_control_values['aggregation'] = aggregation_value

    computation_method_value = 'cumulative'
    new_control_values['computation_method'] = computation_method_value

    if button_clicked == 'duplicate-graph-button':
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        survey_df = pdt.phytobs_df[pdt.phytobs_df['Survey'] == survey_value]
        max_top_taxons = len(survey_df['Taxonomic group'].unique())
        top_taxons_value = controls_by_index[f"index-{active_tab_id_index}"]['top_taxon_group']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        computation_method_value = controls_by_index[f"index-{active_tab_id_index}"]['computation_method']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']

    controls_by_index[f"index-{next_index}"] = new_control_values

    survey_select = dcc.Dropdown(
        id={"type": "top-taxon-group-survey-dropdown", "index": next_index},
        options=survey_options,
        value=survey_value)

    new_survey_dropdown = html.Div([
        dbc.Label("Survey"),
        survey_select
    ])

    new_top_taxon_count = html.Div([
        dbc.Label('Top N Taxon Group:'),
        dbc.Input(
            id={"type": 'top-taxon-group-input', "index": next_index},
            type='number',
            value=top_taxons_value,
            min=1,
            max=max_top_taxons
        ),
    ])

    new_date_picker = html.Div([
        dbc.Label('Date :'),
        dcc.DatePickerRange(
            id={"type": 'top-taxon-group-survey-date-picker', "index": next_index},
            min_date_allowed=min(pdt.phytobs_df['Sampling date']),
            max_date_allowed=max(pdt.phytobs_df['Sampling date']),
            initial_visible_month=max(pdt.phytobs_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value,
        ),
    ])

    new_computation_method_choice = html.Div([
        dbc.Label('Computation Method :'),
        dcc.Dropdown(
            id={"type": 'top-taxon-group-computation-method', "index": next_index},
            options=[
                {'label': 'Average', 'value': 'average'},
                {'label': 'Cumulative', 'value': 'cumulative'},
            ],
            value=computation_method_value
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Graph Type :'),
        dcc.Dropdown(
            id={"type": 'top-taxon-group-graph-type', "index": next_index},
            options=[
                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Stacked histogram', 'value': 'stacked histogram'},
                {'label': 'Box plot', 'value': 'box'},
                {'label': 'Pie chart', 'value': 'pie'}
            ],
            value=graph_type_value
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('Aggregation per :'),
        dcc.RadioItems(
            id={"type": 'top-taxon-group-survey-aggregation', "index": next_index},
            options=[
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'}

            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_survey_dropdown,
            new_top_taxon_count,
            new_date_picker,
            new_computation_method_choice,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"Top PHYTOBS Graph {next_index + 1}")

    patched_children.append(new_tab)
    LOGGER.debug("Done add_remove_top_taxon_group_controls")

    return patched_children, disabled


def add_remove_hydro_controls(next_index, active_tab_index, children, button_clicked):
    disabled = True

    LOGGER.info(f"Generating analyst parameters controls at index {next_index}")

    new_control_values = {'type': 'parameters'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    # Obtenir la liste des enquêtes uniques
    surveys = pdt.analyst_df['Survey'].unique()
    # Définir les options du menu déroulant pour les enquêtes
    survey_options = [{'label': survey, 'value': survey} for survey in surveys]

    survey_value = [surveys[random.randrange(len(survey_options) - 1)]]
    new_control_values['survey'] = survey_value

    parameter_options = [{'label': k, 'value': k} for k in pdt.hydro_units.keys()]
    parameters_value = 'Temperature'
    new_control_values['parameter'] = parameters_value

    start_date_value = min(pdt.hydro_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(pdt.hydro_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'year'
    new_control_values['aggregation'] = aggregation_value

    if button_clicked == 'duplicate-graph-button':
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        parameters_value = controls_by_index[f"index-{active_tab_id_index}"]['parameter']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']

    controls_by_index[f"index-{next_index}"] = new_control_values

    new_parameters_dropdown = html.Div([
        dbc.Label("Parameter :"),
        dcc.Dropdown(
            id={"type": "parameters-dropdown", "index": next_index},
            options=parameter_options,
            value=parameters_value
        )
    ])

    survey_select = dcc.Dropdown(
        id={"type": "hydro-survey-dropdown", "index": next_index},
        options=survey_options,
        value=survey_value,
        multi=True
    )

    new_survey_dropdown = html.Div([
        dbc.Label("Survey :"),
        survey_select
    ])

    new_date_picker = html.Div([
        dbc.Label('Date :'),
        dcc.DatePickerRange(
            id={"type": 'hydro-survey-date-picker', "index": next_index},
            min_date_allowed=min(pdt.hydro_df['Sampling date']),
            max_date_allowed=max(pdt.hydro_df['Sampling date']),
            initial_visible_month=max(pdt.hydro_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Graph Type :'),
        dcc.Dropdown(
            id={"type": 'hydro-graph-type', "index": next_index},
            options=[
                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Box plot', 'value': 'box'},
            ],
            value=graph_type_value
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('Aggregation per :'),
        dcc.RadioItems(
            id={"type": 'hydro-survey-aggregation', "index": next_index},
            options=[
                {'label': 'Sample', 'value': 'sample'},
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'}
            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_parameters_dropdown,
            new_survey_dropdown,
            new_date_picker,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"Hydro-Graph {next_index + 1}")

    patched_children.append(new_tab)

    return patched_children, disabled


@callback(
    Output('graph-panels', 'children'),
    Input('add-analyst-graph-button', 'n_clicks'),
    Input('add-phytobs-graph-button', 'n_clicks'),
    Input('add-top-taxon-graph-button', 'n_clicks'),
    Input('add-top-taxon-group-graph-button', 'n_clicks'),
    Input('add-parameters-graph-button', 'n_clicks'),
    Input('remove-graph-button', 'n_clicks'),
    Input('duplicate-graph-button', 'n_clicks'),
    Input({'type': 'graph-up-button', 'index': ALL}, 'n_clicks'),
    Input({'type': 'graph-down-button', 'index': ALL}, 'n_clicks'),
    State('graph-controls', 'active_tab'),
    State('graph-panels', 'children'),
    State('graph-controls', 'children')
)
def add_remove_graph_panel(n_clicks_add_analyst, n_clicks_add_phytobs, n_clicks_add_top_taxon,
                           n_clicks_add_top_taxon_group,
                           n_clicks_add_parameters, n_clicks_remove, n_clicks_duplicate, n_clicks_graph_up,
                           n_clicks_graph_down,
                           active_tab_index, children, tabs):
    LOGGER.debug("Starting add_remove_graph_panel")

    button_clicked = ctx.triggered_id

    if n_clicks_add_analyst is None:
        n_clicks_add_analyst = 0

    if n_clicks_add_phytobs is None:
        n_clicks_add_phytobs = 0

    if n_clicks_add_parameters is None:
        n_clicks_add_parameters = 0

    if n_clicks_add_top_taxon is None:
        n_clicks_add_top_taxon = 0

    if n_clicks_add_top_taxon_group is None:
        n_clicks_add_top_taxon_group = 0

    if n_clicks_duplicate is None:
        n_clicks_duplicate = 0

    next_index = n_clicks_add_analyst + n_clicks_add_phytobs + n_clicks_add_parameters + n_clicks_add_top_taxon + \
                 n_clicks_add_top_taxon_group + n_clicks_duplicate

    new_graph_type = None
    if button_clicked == 'add-analyst-graph-button':
        new_graph_type = 'analyst'
    if button_clicked == 'add-phytobs-graph-button':
        new_graph_type = 'phytobs'
    if button_clicked == 'add-top-taxon-graph-button':
        new_graph_type = 'top-taxon'
    if button_clicked is None or button_clicked == 'add-top-taxon-group-graph-button':
        new_graph_type = 'top-taxon-group'
    if button_clicked == 'add-parameters-graph-button':
        new_graph_type = 'parameters'

    if button_clicked == 'duplicate-graph-button':
        active_tab_index_int = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index_int]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']
        new_graph_type = controls_by_index[f"index-{active_tab_id_index}"]['type']

    if new_graph_type is not None:
        new_graph_id = {'type': f"{new_graph_type}-graph", 'index': next_index}
        new_taxon_count_graph = dbc.Col(html.Div([
            dcc.Graph(
                id=new_graph_id
            ),
        ]))
        patched_children = Patch()
        button_div = html.Div(
            className="h-100 d-flex align-items-center justify-content-center",
            children=[
                dbc.Button(
                    id={'type': 'graph-up-button', 'index': next_index},
                    className="bi bi-arrow-up",
                    color="btn btn-light btn-sm"
                ),
                dbc.Button(
                    id={'type': 'graph-down-button', 'index': next_index},
                    className="bi bi-arrow-down",
                    color="btn btn-light btn-sm"
                ),
                dbc.Tooltip(
                    "Move Graph Up",
                    target={'type': 'graph-up-button', 'index': next_index},
                ),
                dbc.Tooltip(
                    "Move Graph Down",
                    target={'type': 'graph-down-button', 'index': next_index},
                )

            ])
        new_graph_panel = dbc.Row(
            id={'type': 'graph-row', 'index': next_index},
            children=[new_taxon_count_graph, button_div],
        )
        patched_children.append(new_graph_panel)

    if button_clicked == 'remove-graph-button' and active_tab_index:
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = tabs[active_tab_index]
        active_tab_id = active_tab['props']['id']
        LOGGER.info(f"Remove Graph Panel - Active index: {active_tab_id}")
        patched_children = []
        for child in children:
            child_id = child['props']['id']
            LOGGER.info(f"Child id {child_id}")
            if child_id['type'] == 'graph-row' and child_id['index'] == active_tab_id['index']:
                continue
            patched_children.append(child)

    if button_clicked is not None and 'type' in button_clicked and (
            button_clicked['type'] == 'graph-up-button' or button_clicked['type'] == 'graph-down-button'):
        patched_children = children
        button_index = button_clicked['index']
        source_index = None
        source_child = None
        for index in range(0, len(children)):
            child = children[index]
            child_id = child['props']['id']
            if child_id['type'] == 'graph-row' and child_id['index'] == button_index:
                source_index = index
                source_child = child

        if source_child:
            destination_index = source_index
            if button_clicked['type'] == 'graph-up-button':
                destination_index = source_index - 1
            if button_clicked['type'] == 'graph-down-button':
                destination_index = source_index + 1

            LOGGER.info(f"Attempting to move graph from {source_index} to {destination_index}")
            if source_index >= 0 and source_index < len(
                    children) and destination_index >= 0 and destination_index < len(children):
                destination_child = children[destination_index]
                children[destination_index] = source_child
                children[source_index] = destination_child
            else:
                LOGGER.info(f"Indexes are outside boundaries [0,{len(children) - 1}]. No changes performed.")

    LOGGER.debug("Done add_remove_graph_panel")

    return patched_children


@callback(
    Output({'type': 'analyst-taxon-dropdown', 'index': MATCH}, 'options'),
    Input({'type': 'analyst-survey-dropdown', 'index': MATCH}, 'value'),
    State({'type': 'analyst-survey-dropdown', 'index': MATCH}, 'id')
)
def update_taxon_dropdown(survey, id):
    LOGGER.debug("Starting update_taxon_dropdown")
    survey_analyst_df = pdt.analyst_df[pdt.analyst_df['Survey'] == survey]
    LOGGER.debug("Done update_taxon_dropdown")
    return sorted(survey_analyst_df['Accepted taxon name'].unique())


@callback(
    Output({'type': 'phytobs-taxon-dropdown', 'index': MATCH}, 'options'),
    Input({'type': 'phytobs-survey-dropdown', 'index': MATCH}, 'value'),
    State({'type': 'phytobs-survey-dropdown', 'index': MATCH}, 'id')
)
def update_taxon_group_dropdown(survey, id):
    LOGGER.debug("Starting update_taxon_group_dropdown")
    survey_phytobs_df = pdt.phytobs_df[pdt.phytobs_df['Survey'] == survey]
    LOGGER.debug("Done update_taxon_group_dropdown")
    return _build_taxon_group_dropdown_options(survey_phytobs_df)


@callback(
    Output({"type": 'top-taxon-input', "index": MATCH}, 'max'),
    Input({"type": 'top-taxon-survey-dropdown', "index": MATCH}, 'value'),
    State({"type": 'top-taxon-input', "index": MATCH}, 'id')
)
def update_top_taxon_count_max(survey, id):
    LOGGER.debug("Starting update_top_taxon_count_max")
    survey_analyst_df = pdt.analyst_df[pdt.analyst_df['Survey'] == survey]
    LOGGER.debug("Done update_top_taxon_count_max")
    return len(survey_analyst_df['Accepted taxon name'].unique())


@callback(
    Output({"type": 'top-taxon-group-input', "index": MATCH}, 'max'),
    Input({"type": 'top-taxon-group-survey-dropdown', "index": MATCH}, 'value'),
    State({"type": 'top-taxon-group-input', "index": MATCH}, 'id')
)
def update_top_taxon_group_count_max(survey, id):
    LOGGER.debug("Starting update_top_taxon_count_group_max")
    survey_phytobs_df = pdt.phytobs_df[pdt.phytobs_df['Survey'] == survey]
    LOGGER.debug("Done update_top_taxon_count_group_max")
    return len(survey_phytobs_df['Taxonomic group'].unique())


@callback(
    Output({'type': 'analyst-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'analyst-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'analyst-taxon-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'analyst-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'analyst-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'analyst-survey-aggregation', 'index': MATCH}, 'value'),
    Input({"type": 'analyst-graph-type', "index": MATCH}, 'value'),
    State({'type': 'analyst-taxon-dropdown', 'index': MATCH}, 'id')
)
def update_analyst_graph(survey, selected_taxa, start_date, end_date, aggregation, graph_type, id):
    """
    Update the analyst graph based on the selected survey, taxon, date range, aggregation, and graph type.

    :param survey: Selected survey value from the dropdown.
    :param selected_taxa: List of selected taxon names from the dropdown.
    :param start_date: Start date selected from the date picker.
    :param end_date: End date selected from the date picker.
    :param aggregation: Aggregation type selected from the dropdown.
    :param graph_type: Graph type selected from the dropdown.
    :param id: ID of the analyst taxon dropdown.
    :return: Updated analyst graph figure.
    """

    LOGGER.debug("Starting update_analyst_graph")

    # Filter the dataframe based on the selected taxonomic groups and date range
    if selected_taxa is None:
        selected_taxa = []

    if start_date is None:
        start_date = datetime.date(1900, 1, 1)
    else:
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()

    if end_date is None:
        end_date = datetime.date(2900, 12, 31)
    else:
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

    LOGGER.info(f"Date type: {type(start_date)}, {start_date}")

    if survey is None:
        survey = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = survey
        controls_by_index[f"index-{control_index}"]['taxons'] = selected_taxa
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation

    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")

    filtered_df = pdt.analyst_df[
        (pdt.analyst_df['Survey'] == survey) &
        (pdt.analyst_df['Sampling datetime'].dt.date >= start_date) &
        (pdt.analyst_df['Sampling datetime'].dt.date <= end_date) &
        (pdt.analyst_df['Accepted taxon name'].isin(selected_taxa))
        ].copy()

    if graph_type == 'pie':
        # Group the dataframe by taxon name and take the mean of the counts.
        pie_data = filtered_df.groupby('Accepted taxon name', as_index=False)['Count'].mean()
        fig = px.pie(pie_data, names='Accepted taxon name',
                     values='Count',
                     title=f"Analyst-Graph {id['index'] + 1}: Occurrence Count of selected analyst taxa in survey {survey} ", )

    elif graph_type == 'box':
        time_group = 'Sampling date'
        if aggregation == 'year':
            time_group = 'Year'
        elif aggregation == 'month':
            time_group = 'Month'
        elif aggregation == 'month per year':
            time_group = 'Year month'
        elif aggregation == 'season':
            time_group = 'Season'
        fig = px.box(
            filtered_df,
            x=time_group,
            y='Count',
            color='Accepted taxon name',
            title=f"Analyst-Graph {id['index'] + 1}: Occurrence distribution of selected analyst taxa in survey {survey} ",
        )
        fig.update_yaxes(title="Avg. individuals per (cells/l)")

    else:
        traces = []
        for sp in selected_taxa:
            LOGGER.info(f"Starting update chart for selected taxon: {sp}")
            sp_df = filtered_df[filtered_df['Accepted taxon name'] == sp].copy()
            if sp_df.empty:
                LOGGER.info(f"No data for selected taxon: {sp}")
                continue
            LOGGER.info(f"Done update chart for selected taxon: {sp}")
            if aggregation == 'sample':
                x_values = sp_df['Sampling datetime'].dt.date
                # Arbitrarily fix bar width to 30 days (expressed in milliseconds).
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True, width=1000 * 60 * 60 * 24 * 30)
            elif aggregation == 'year':
                sp_df = sp_df.groupby(
                    ['Year', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Year']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'month':
                sp_df = sp_df.groupby(
                    ['Month', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Month']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'month per year':
                sp_df = sp_df.groupby(
                    ['Year month', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Year month']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'season':
                sp_df = sp_df.groupby(
                    ['Season', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Season']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            traces.append(trace)

        layout = go.Layout(
            title=f"Analyst-Graph {id['index'] + 1}: Occurrences of selected analyst taxa in survey {survey} ",
            xaxis={'title': 'Sampling date'}, yaxis={'title': 'Avg. individuals per (cells/l)'})

        fig = go.Figure(data=traces, layout=layout)
        if aggregation == 'sample':
            fig.update_xaxes(type='date', range=[start_date, end_date])
        if aggregation == 'year':
            start_year = start_date.year
            end_year = end_date.year
            categories = [*range(start_year, end_year + 1)]
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0, len(categories)])

        if aggregation == 'season':
            fig.update_xaxes(type='category', categoryorder='array',
                             categoryarray=['Winter', 'Spring', 'Summer', 'Autumn'],
                             range=[-0.5, 3.5])
        if aggregation == 'month':
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=list(calendar.month_name)[1:],
                             range=[-0.5, 11.5])
        if aggregation == 'month per year':
            start_year = start_date.year
            end_year = end_date.year
            categories = []
            for year in range(start_year, end_year + 1):
                for month in range(1, 13):
                    categories.append(f"{year:04d}-{month:02d}")
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0, len(categories)])

    LOGGER.debug("Done update_analyst_graph")
    return fig


@callback(
    Output({'type': 'phytobs-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'phytobs-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'phytobs-taxon-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'phytobs-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'phytobs-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'phytobs-survey-aggregation', 'index': MATCH}, 'value'),
    Input({"type": 'phytobs-graph-type', "index": MATCH}, 'value'),
    State({'type': 'phytobs-taxon-dropdown', 'index': MATCH}, 'id')
)
def update_phytobs_graph(survey, selected_taxa, start_date, end_date, aggregation, graph_type, id):
    """
    Update the phytobs graph based on the selected survey, taxon, date range, aggregation, and graph type.

    :param survey: Selected survey value from the dropdown.
    :param selected_taxa: List of selected taxon names from the dropdown.
    :param start_date: Start date selected from the date picker.
    :param end_date: End date selected from the date picker.
    :param aggregation: Aggregation type selected from the dropdown.
    :param graph_type: Graph type selected from the dropdown.
    :param id: ID of the phytobs taxon dropdown.
    :return: Updated phytobs graph figure.
    """
    LOGGER.debug(f"Starting update_phytobs_graph.")

    # Filter the dataframe based on the selected taxonomic groups and date range
    if selected_taxa is None:
        selected_taxa = []

    if start_date is None:
        start_date = datetime.date(1900, 1, 1)
    else:
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()

    if end_date is None:
        end_date = datetime.date(2900, 12, 31)
    else:
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

    LOGGER.info(f"Date type: {type(start_date)}, {start_date}")

    if survey is None:
        survey = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = survey
        controls_by_index[f"index-{control_index}"]['taxons'] = selected_taxa
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation

    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")

    filtered_df = pdt.phytobs_df[
        (pdt.phytobs_df['Survey'] == survey) &
        (pdt.phytobs_df['Sampling datetime'].dt.date >= start_date) &
        (pdt.phytobs_df['Sampling datetime'].dt.date <= end_date) &
        (pdt.phytobs_df['Taxonomic group'].isin(selected_taxa))
        ].copy()

    if graph_type == 'pie':
        # Group the dataframe by taxon name and take the mean of the counts.
        pie_data = filtered_df.groupby('Taxonomic group', as_index=False)['Count'].mean()
        fig = px.pie(pie_data, names='Taxonomic group',
                     values='Count',
                     title=f"PHYTOBS-Graph {id['index'] + 1}: Occurrence Count of selected PHYTOBS taxon groups in survey {survey} ", )
        fig.update_layout(legend_orientation='h', legend_title_text="Taxonomic group")

    elif graph_type == 'box':
        time_group = 'Sampling date'
        if aggregation == 'year':
            time_group = 'Year'
        elif aggregation == 'month':
            time_group = 'Month'
        elif aggregation == 'month per year':
            time_group = 'Year month'
        elif aggregation == 'season':
            time_group = 'Season'
        fig = px.box(
            filtered_df,
            x=time_group,
            y='Count',
            color='Taxonomic group',
            title=f"PHYTOBS-Graph {id['index'] + 1}: Occurrence distribution of PHYTOBS taxon groups in survey {survey} ",
        )
        fig.update_yaxes(
            legend_orientation='h',
            legend_title_text="Taxonomic group",
            title="Avg. individuals per (cells/l)")

    else:
        traces = []
        for sp in selected_taxa:
            LOGGER.info(f"Starting update chart for selected taxon: {sp}")
            sp_df = filtered_df[filtered_df['Taxonomic group'] == sp].copy()
            if sp_df.empty:
                LOGGER.info(f"No data for selected taxon: {sp}")
                continue
            LOGGER.info(f"Done update chart for selected taxon: {sp}")
            if aggregation == 'sample':
                x_values = sp_df['Sampling datetime'].dt.date
                # Arbitrarily fix bar width to 30 days (expressed in milliseconds).
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True, width=1000 * 60 * 60 * 24 * 30)
            elif aggregation == 'year':
                sp_df = sp_df.groupby(
                    ['Year', 'Taxonomic group']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Year']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'month':
                sp_df = sp_df.groupby(
                    ['Month', 'Taxonomic group']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Month']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'month per year':
                sp_df = sp_df.groupby(
                    ['Year month', 'Taxonomic group']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Year month']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'season':
                sp_df = sp_df.groupby(
                    ['Season', 'Taxonomic group']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Season']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            traces.append(trace)

        layout = go.Layout(
            title=f"PHYTOBS-Graph {id['index'] + 1}: Occurrences of PHYTOBS taxon groups in survey {survey} ",
            xaxis={'title': 'Sampling date'}, yaxis={'title': 'Avg. individuals per (cells/l)'},
        )

        fig = go.Figure(data=traces, layout=layout)
        fig.update_layout(
            legend_orientation='h',
        )

        if aggregation == 'sample':
            fig.update_xaxes(type='date', range=[start_date, end_date])
        if aggregation == 'year':
            start_year = start_date.year
            end_year = end_date.year
            categories = [*range(start_year, end_year + 1)]
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0, len(categories)])

        if aggregation == 'season':
            fig.update_xaxes(type='category', categoryorder='array',
                             categoryarray=['Winter', 'Spring', 'Summer', 'Autumn'],
                             range=[-0.5, 3.5])
        if aggregation == 'month':
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=list(calendar.month_name)[1:],
                             range=[-0.5, 11.5])
        if aggregation == 'month per year':
            start_year = start_date.year
            end_year = end_date.year
            categories = []
            for year in range(start_year, end_year + 1):
                for month in range(1, 13):
                    categories.append(f"{year:04d}-{month:02d}")
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0, len(categories)])

    LOGGER.debug("Done update_phytobs_graph")
    return fig


@callback(
    Output({'type': 'top-taxon-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'top-taxon-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-graph-type', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'top-taxon-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'top-taxon-survey-aggregation', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-computation-method', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-input', 'index': MATCH}, 'value'),
    State({'type': 'top-taxon-input', 'index': MATCH}, 'id')

)
def update_top_taxon_graph(survey, graph_type, start_date, end_date, aggregation, computation_method, top_taxon_input, id):
    """
    Update the top taxon graph based on the selected survey, graph type, date range, aggregation, and top taxon count.

    :param survey: Selected survey value from the dropdown.
    :param graph_type: Graph type selected from the dropdown.
    :param start_date: Start date selected from the date picker.
    :param end_date: End date selected from the date picker.
    :param aggregation: Aggregation type selected from the dropdown.
    :param top_taxon_input: Top taxon count input value.
    :param id: ID of the top taxon survey dropdown.
    :return: Updated top taxon graph figure.
    """
    LOGGER.debug("Starting update_top_taxon_graph")

    if top_taxon_input is None:
        top_taxon_input = []

    if start_date is None:
        start_date = datetime.date(1900, 1, 1)
    else:
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()

    if end_date is None:
        end_date = datetime.date(2900, 12, 31)
    else:
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

    LOGGER.info(f"Date type: {type(start_date)}, {start_date}")

    if survey is None:
        survey = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = survey
        controls_by_index[f"index-{control_index}"]['top_taxon'] = top_taxon_input
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation

    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")
    filtered_df = pdt.analyst_df[
        (pdt.analyst_df['Survey'] == survey) &
        (pdt.analyst_df['Sampling datetime'].dt.date >= start_date) &
        (pdt.analyst_df['Sampling datetime'].dt.date <= end_date)
        ]

    time_group = 'Sampling date'
    if aggregation == 'year':
        time_group = 'Year'
    elif aggregation == 'month':
        time_group = 'Month'
    elif aggregation == 'month per year':
        time_group = 'Year month'
    elif aggregation == 'season':
        time_group = 'Season'

    # Determine the most abundant taxons (cumulative or average) in the given survey and time interval
    if computation_method == 'cumulative':
        top_taxons_df = filtered_df.groupby('Accepted taxon name',as_index=False)['Count'].sum()
    if computation_method == 'average':
        top_taxons_df = filtered_df.groupby('Accepted taxon name', as_index=False)['Count'].mean()

    top_taxons_df.sort_values(by=['Count'],inplace=True,ascending=False)
    top_taxons = top_taxons_df.iloc[0:top_taxon_input]['Accepted taxon name'].tolist()

    # Build a dataframe containing only data for the most abundant taxons.
    top_taxons_df = filtered_df[filtered_df['Accepted taxon name'].isin(top_taxons)].copy()

    # Build a dataframe for taxons not belonging to the topmost taxons.
    other_taxons_df = filtered_df[~filtered_df['Accepted taxon name'].isin(top_taxons)].copy()
    # Force their taxon names to 'Others'
    other_taxons_df['Accepted taxon name'] = 'Others'
    # Group the 'Others' taxons by sampling date
    other_taxons_df = other_taxons_df.groupby([time_group,'Accepted taxon name'])['Count'].mean()
    other_taxons_df = other_taxons_df.reset_index()
    # Build the dataframe to be used for the graph by combining the dataframe with
    # with the topmost taxa counts and the dataframe with the 'Others' counts.
    filtered_df = pd.concat([top_taxons_df, other_taxons_df])

    if graph_type == 'pie':
        fig = px.pie(filtered_df, names='Accepted taxon name', labels={'Accepted taxon name': 'Accepted taxon name'},
                     title=f"Top Analyst Graph {graph_type} {id['index'] + 1}: Top {top_taxon_input} analyst taxa in survey {survey}")
        fig.update_layout(legend_title_text=(f"Top {top_taxon_input} Taxons"),
                          title="Avg. individuals per (cells/l)")

    elif graph_type == 'box':
        fig = px.box(
            filtered_df,
            x=time_group,
            y='Count',
            color='Accepted taxon name',
            title=f"Top Analyst Graph {id['index'] + 1}: Top {top_taxon_input} analyst taxa in survey {survey}",
        )
        fig.update_yaxes(title="Avg. individuals per (cells/l)")
        fig.update_layout(
            xaxis=dict(title='Sampling Date', ),
            legend_title_text=(f"Top {top_taxon_input} Taxons")
        )

    elif graph_type == 'histogram':
        fig = px.histogram(filtered_df, x=time_group, y='Count', color='Accepted taxon name',
                           labels={'Accepted taxon name': 'Accepted taxon name'},
                           histfunc="avg",
                           title=f"Top Analyst Graph {id['index'] + 1}: Top {top_taxon_input} analyst taxa in survey {survey}")
        fig.update_layout(
            xaxis=dict(title='Sampling Date', ),
            yaxis=dict(title='Avg. individuals per (cells/l)'),
            legend_title_text=(f"Top {top_taxon_input} Taxa"),
            barmode='group'  # Modifier le barmode pour obtenir un histogramme non empilé
        )

    elif graph_type == 'stacked histogram':
        fig = px.histogram(filtered_df, x=time_group, y='Count', color='Accepted taxon name',
                           histfunc="avg",
                           barmode='stack', labels={'Accepted taxon name': 'Accepted taxon name'},
                           title=f"Top Analyst Graph {id['index'] + 1}: Top {top_taxon_input} analyst taxa in survey {survey}")
        fig.update_yaxes(title="Avg. individuals per (cells/l)")
        fig.update_layout(
            xaxis=dict(title='Sampling Date', ),
            legend_title_text=(f"Top {top_taxon_input} Taxa"),
            barmode='stack',
            bargap=0.2
        )

    if aggregation == 'sample':
        fig.update_xaxes(type='date', range=[start_date, end_date])
    if aggregation == 'year':
        start_year = start_date.year
        end_year = end_date.year
        categories = [*range(start_year, end_year + 1)]
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                         range=[0, len(categories)])

    if aggregation == 'season':
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=['Winter', 'Spring', 'Summer', 'Autumn'],
                         range=[-0.5, 3.5])
    if aggregation == 'month':
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=list(calendar.month_name)[1:],
                         range=[-0.5, 11.5])
    if aggregation == 'month per year':
        start_year = start_date.year
        end_year = end_date.year
        categories = []
        for year in range(start_year, end_year + 1):
            for month in range(1, 13):
                categories.append(f"{year:04d}-{month:02d}")
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                         range=[0, len(categories)])

    LOGGER.debug("Done update_top_taxon_graph")
    return fig


@callback(
    Output({'type': 'top-taxon-group-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'top-taxon-group-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-group-graph-type', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-group-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'top-taxon-group-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'top-taxon-group-survey-aggregation', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-group-computation-method', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-group-input', 'index': MATCH}, 'value'),
    State({'type': 'top-taxon-group-input', 'index': MATCH}, 'id')

)
def update_top_taxon_group_graph(survey, graph_type, start_date, end_date, aggregation, computation_method, top_taxon_input, id):
    """
    Update the top taxon graph based on the selected survey, graph type, date range, aggregation, and top taxon count.

    :param survey: Selected survey value from the dropdown.
    :param graph_type: Graph type selected from the dropdown.
    :param start_date: Start date selected from the date picker.
    :param end_date: End date selected from the date picker.
    :param aggregation: Aggregation type selected from the dropdown.
    :param top_taxon_input: Top taxon count input value.
    :param id: ID of the top taxon survey dropdown.
    :return: Updated top taxon graph figure.
    """
    LOGGER.debug("Starting update_top_taxon_group_graph")
    if top_taxon_input is None:
        top_taxon_input = []

    if start_date is None:
        start_date = datetime.date(1900, 1, 1)
    else:
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()

    if end_date is None:
        end_date = datetime.date(2900, 12, 31)
    else:
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

    LOGGER.info(f"Date type: {type(start_date)}, {start_date}")

    if survey is None:
        survey = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = survey
        controls_by_index[f"index-{control_index}"]['top_taxon_group'] = top_taxon_input
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation

    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")
    filtered_df = pdt.phytobs_df[
        (pdt.phytobs_df['Survey'] == survey) &
        (pdt.phytobs_df['Sampling datetime'].dt.date >= start_date) &
        (pdt.phytobs_df['Sampling datetime'].dt.date <= end_date)
        ]

    time_group = 'Sampling date'
    if aggregation == 'year':
        time_group = 'Year'
    elif aggregation == 'month':
        time_group = 'Month'
    elif aggregation == 'month per year':
        time_group = 'Year month'
    elif aggregation == 'season':
        time_group = 'Season'

    # Determine the most abundant taxons (cumulative or average) in the given survey and time interval
    if computation_method == 'cumulative':
        top_taxons_df = filtered_df.groupby('Taxonomic group',as_index=False)['Count'].sum()
    if computation_method == 'average':
        top_taxons_df = filtered_df.groupby('Taxonomic group',as_index=False)['Count'].mean()

    top_taxons_df.sort_values(by=['Count'],inplace=True,ascending=False)
    top_taxons = top_taxons_df.iloc[0:top_taxon_input]['Taxonomic group'].tolist()

    # Build a dataframe containing only data for the most abundant taxons.
    top_taxons_df = filtered_df[filtered_df['Taxonomic group'].isin(top_taxons)].copy()

    # Build a dataframe for taxons not belonging to the topmost taxons.
    other_taxons_df = filtered_df[~filtered_df['Taxonomic group'].isin(top_taxons)].copy()
    # Force their taxon names to 'Others'
    other_taxons_df['Taxonomic group'] = 'Others'
    # Group the 'Others' taxons by sampling date
    other_taxons_df = other_taxons_df.groupby([time_group,'Taxonomic group'])['Count'].mean()
    other_taxons_df = other_taxons_df.reset_index()
    # Build the dataframe to be used for the graph by combining the dataframe with
    # with the topmost taxa counts and the dataframe with the 'Others' counts.
    filtered_df = pd.concat([top_taxons_df, other_taxons_df])

    if graph_type == 'pie':
        fig = px.pie(filtered_df, names='Taxonomic group', labels={'Taxonomic group': 'Taxonomic group'},
                     title=f"Top PHYTOBS Graph {id['index'] + 1}: Top {top_taxon_input} PHYTOBS taxon groups in survey {survey}")
        fig.update_layout(
            legend_orientation='h',
            legend_title_text=(f"Top {top_taxon_input} PHYTOBS Taxon Groups"),
            title="Avg. individuals per (cells/l)")

    elif graph_type == 'box':
        fig = px.box(
            filtered_df,
            x=time_group,
            y='Count',
            color='Taxonomic group',
            title=f"Top PHYTOBS Graph {id['index'] + 1}: Top {top_taxon_input} PHYTOBS taxon groups in survey {survey}",
        )
        fig.update_yaxes(title="Avg. individuals per (cells/l)")
        fig.update_layout(
            legend_orientation='h',
            xaxis=dict(title='Sampling Date', ),
            legend_title_text=(f"Top {top_taxon_input} PHYTOBS Taxon Groups")
        )

    elif graph_type == 'histogram':
        fig = px.histogram(filtered_df, x=time_group, y='Count', color='Taxonomic group',
                           labels={'Taxonomic group': 'Taxonomic group'},
                           histfunc="avg",
                           title=f"Top PHYTOBS Graph {id['index'] + 1}: Top {top_taxon_input} PHYTOBS taxon groups in survey {survey}")
        fig.update_layout(
            xaxis=dict(title='Sampling Date', ),
            yaxis=dict(title='Avg. individuals per (cells/l)'),
            legend_orientation='h',
            legend_title_text=(f"Top {top_taxon_input} PHYTOBS Taxon Groups"),
            barmode='group'  # Modifier le barmode pour obtenir un histogramme non empilé
        )

    elif graph_type == 'stacked histogram':
        fig = px.histogram(filtered_df, x=time_group, y='Count', color='Taxonomic group',
                           barmode='stack', labels={'Taxonomic group': 'Taxonomic group'},
                           histfunc="avg",
                           title=f"Top PHYTOBS Graph {id['index'] + 1}: Top {top_taxon_input} PHYTOBS taxon groups in survey {survey}")
        fig.update_yaxes(title="Avg. individuals per (cells/l)")
        fig.update_layout(
            xaxis=dict(title='Sampling Date', ),
            legend_orientation='h',
            legend_title_text=(f"Top {top_taxon_input} PHYTOBS Taxon Groups"),
            barmode='stack',
            bargap=0.2
        )

    if aggregation == 'sample':
        fig.update_xaxes(type='date', range=[start_date, end_date])
    if aggregation == 'year':
        start_year = start_date.year
        end_year = end_date.year
        categories = [*range(start_year, end_year + 1)]
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                         range=[0, len(categories)])

    if aggregation == 'season':
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=['Winter', 'Spring', 'Summer', 'Autumn'],
                         range=[-0.5, 3.5])
    if aggregation == 'month':
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=list(calendar.month_name)[1:],
                         range=[-0.5, 11.5])
    if aggregation == 'month per year':
        start_year = start_date.year
        end_year = end_date.year
        categories = []
        for year in range(start_year, end_year + 1):
            for month in range(1, 13):
                categories.append(f"{year:04d}-{month:02d}")
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                         range=[0, len(categories)])

    LOGGER.debug("Done update_top_taxon_group_graph")
    return fig


@callback(
    Output({'type': 'parameters-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'hydro-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'parameters-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'hydro-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'hydro-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'hydro-survey-aggregation', 'index': MATCH}, 'value'),
    Input({"type": 'hydro-graph-type', "index": MATCH}, 'value'),
    State({'type': 'hydro-survey-dropdown', 'index': MATCH}, 'id')
)
def update_hydro_graph(selected_surveys, selected_parameter, start_date, end_date, aggregation, graph_type, id):
    # Filtrer les données en fonction des enquêtes sélectionnées
    if selected_surveys is None:
        selected_surveys = []

    if start_date is None:
        start_date = datetime.date(1900, 1, 1)
    else:
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()

    if end_date is None:
        end_date = datetime.date(2900, 12, 31)
    else:
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

    if selected_parameter is None:
        selected_parameter = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = selected_surveys
        controls_by_index[f"index-{control_index}"]['parameter'] = selected_parameter
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation
    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")

    hydro_df_tmp = pdt.hydro_df[
        (pdt.hydro_df['Survey'].isin(selected_surveys)) &
        (pdt.hydro_df['Sampling datetime'].dt.date >= start_date) &
        (pdt.hydro_df['Sampling datetime'].dt.date <= end_date)
        ]

    filtered_hydro_df = hydro_df_tmp.copy()

    if graph_type == 'box':
        time_group = 'Sampling date'
        if aggregation == 'year':
            time_group = 'Year'
        elif aggregation == 'month':
            time_group = 'Month'
        elif aggregation == 'month per year':
            time_group = 'Year month'
        elif aggregation == 'season':
            time_group = 'Season'
        fig = px.box(
            filtered_hydro_df,
            x=time_group,
            y=selected_parameter,
            color='Survey',
            title=f"Hydro-Graph {id['index'] + 1}: Evolution of {selected_parameter}.",
        )
        fig.update_yaxes(title=pdt.hydro_units[selected_parameter])

    else:
        traces = []
        for sv in selected_surveys:
            LOGGER.info(f"Starting update chart for selected survey: {sv}")
            sv_df = filtered_hydro_df[filtered_hydro_df['Survey'] == sv].copy()
            if sv_df.empty:
                LOGGER.info(f"No data for selected survey: {sv}")
                continue
            LOGGER.info(f"Done update chart for selected survey: {sv}")
            if aggregation == 'sample':
                x_values = sv_df['Sampling datetime'].dt.date
                # Arbitrarily fix bar width to 30 days (expressed in milliseconds).
                trace = go.Bar(x=x_values, y=sv_df[selected_parameter], name=sv, showlegend=True,
                               width=1000 * 60 * 60 * 24 * 30)
            elif aggregation == 'year':
                sv_df = sv_df.groupby(
                    ['Year', 'Survey']).agg({selected_parameter: 'mean'})
                sv_df = sv_df.reset_index()
                x_values = sv_df['Year']
                trace = go.Bar(x=x_values, y=sv_df[selected_parameter], name=sv, showlegend=True)
            elif aggregation == 'month':
                sv_df = sv_df.groupby(
                    ['Month', 'Survey']).agg({selected_parameter: 'mean'})
                sv_df = sv_df.reset_index()
                x_values = sv_df['Month']
                trace = go.Bar(x=x_values, y=sv_df[selected_parameter], name=sv, showlegend=True)
            elif aggregation == 'month per year':
                sv_df = sv_df.groupby(
                    ['Year month', 'Survey']).agg({selected_parameter: 'mean'})
                sv_df = sv_df.reset_index()
                x_values = sv_df['Year month']
                trace = go.Bar(x=x_values, y=sv_df[selected_parameter], name=sv, showlegend=True)
            elif aggregation == 'season':
                sv_df = sv_df.groupby(
                    ['Season', 'Survey']).agg({selected_parameter: 'mean'})
                sv_df = sv_df.reset_index()
                x_values = sv_df['Season']
                trace = go.Bar(x=x_values, y=sv_df[selected_parameter], name=sv, showlegend=True)
            traces.append(trace)

        layout = go.Layout(
            title=f"Hydro-Graph {id['index'] + 1}: Evolution of {selected_parameter}. ",
            xaxis={'title': 'Sampling date'}, yaxis={'title': pdt.hydro_units[selected_parameter]})

        fig = go.Figure(data=traces, layout=layout)
        if aggregation == 'sample':
            fig.update_xaxes(type='date', range=[start_date, end_date])
        if aggregation == 'year':
            start_year = start_date.year
            end_year = end_date.year
            categories = [*range(start_year, end_year + 1)]
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0, len(categories)])

        if aggregation == 'season':
            fig.update_xaxes(type='category', categoryorder='array',
                             categoryarray=['Winter', 'Spring', 'Summer', 'Autumn'],
                             range=[-0.5, 3.5])
        if aggregation == 'month':
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=list(calendar.month_name)[1:],
                             range=[-0.5, 11.5])
        if aggregation == 'month per year':
            start_year = start_date.year
            end_year = end_date.year
            categories = []
            for year in range(start_year, end_year + 1):
                for month in range(1, 13):
                    categories.append(f"{year:04d}-{month:02d}")
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0, len(categories)])

    return fig


graph_control_tabs = dbc.Tabs(id="graph-controls", children=[], active_tab='tab-0')

layout = html.Div([
    html.Div([
        html.Div([
            dbc.Button(
                children=[html.I("", className="bi bi-plus"), html.Span("Analyst")],
                id="add-analyst-graph-button",
                n_clicks=0,
                className="btn btn-success"
            ),

            dbc.Button(
                children=[html.I("", className="bi bi-plus"), html.Span("PHYTOBS")],
                id="add-phytobs-graph-button",
                n_clicks=0,
                className="btn btn-success"
            ),

            dbc.Button(
                children=[html.I("", className="bi bi-plus"), html.Span("Hydro")],
                id="add-parameters-graph-button",
                n_clicks=0,
                className="btn btn-success"
            ),

            dbc.Button(
                children=[html.I("", className="bi bi-plus"), html.Span("Top Analyst")],
                id="add-top-taxon-graph-button",
                n_clicks=0,
                className="btn btn-primary"
            ),
            dbc.Button(
                children=[html.I("", className="bi bi-plus"), html.Span("Top PHYTOBS")],
                id="add-top-taxon-group-graph-button",
                n_clicks=0,
                className="btn btn-primary"
            ),
        ]),
        html.Div([
            graph_control_tabs,
            html.Div([
                dbc.Button(
                    "Remove",
                    id="remove-graph-button",
                    n_clicks=0,
                    disabled=True,
                    color="danger",
                    className="bd-control-button",
                ),
                dbc.Button(
                    "Duplicate",
                    id='duplicate-graph-button',
                    color="secondary",
                    className="bd-control-button",
                ),
            ], className="bd-control-buttons")
        ], className="bd-controls"),
    ], id='navbar', className="bd-navbar"),

    # Main content
    html.Div([
        html.Details(
            open=True,
            children=[
                html.Summary("PHYTOBS SNO Sampling Station Maps"),
                dl.Map([
                    dl.TileLayer(),
                    dl.LayerGroup(id='marker-layer'),
                    dl.FullscreenControl(position='topleft'),
                    dl.LayersControl(
                        [
                            dl.BaseLayer(dl.TileLayer(
                                url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"),
                                name='Esri WorldImagery', checked=True),
                            dl.BaseLayer(dl.TileLayer(), name='Base Map'),
                            dl.BaseLayer(dl.TileLayer(url="https://tile.openstreetmap.bzh/br/{z}/{x}/{y}.png"),
                                         name='OpenStreetMap'),
                            dl.BaseLayer(dl.TileLayer(
                                url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"),
                                name='Esri WorldTopoMap'),
                            dl.Overlay(dl.MarkerClusterGroup(id='survey-info-layer'), name='Survey Information',
                                       checked=True),
                            dl.Overlay(dl.MarkerClusterGroup(id='analyst-taxon-layer'), name='Top 5 Taxons',
                                       checked=False),
                            dl.Overlay(dl.MarkerClusterGroup(id='phytobs-taxon-layer'), name='Top 5 Taxon Groups',
                                       checked=False)

                        ]
                    )
                ],
                    center=(46.59493039157112, 2.4178988109021606),
                    style={'max-width': '100%', 'height': '400px', 'position': 'center'},
                    zoom=5,
                )]),
        html.Hr(className='bd-hr'),
        html.Div(id="graph-panels", children=[]),
        html.Div(id='marker-popup', style={'display': 'none'}),
    ], className='bd-content'),
])
