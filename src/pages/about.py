import dash
from dash import html, dcc

dash.register_page(__name__, order=2)

layout = html.Div(
    dcc.Markdown('''
#### What is the SNO PHYTOBS Data Viewer?

The SNO PHYTOBS Data Viewer has been develop to allow easy visualisation of data collected by the PHYTOBS National
Observation Service (SNO). All the details about how data is collected, processed and made accessible can be found on
the official [PHYTOBS Web site](https://www.phytobs.fr/).

#### Where does the data come from?

Data used to build the graphical representations is pulled directly from the [PHYTOBS data download Web site](https://data.phytobs.fr).
More specifically, data for macrofauna and granulometry are retrieved from the directory containing the aggregated data
for all stations and for the latest release (https://data.phytobs.fr/files/latest/all_sites/)

#### Credits

Development of the PHYTOBS Data Viewer has been funded by the [ILICO](https://ir-ilico.fr) Research Infrastructure. It
been carried out between March and July 2023 during the internship of Fatima Ezzahraa EL HOUJJAJI, under the supervision
of Mark HOEBEKE.
The application is hosted on the computing infrastructure of the [ABiMS](https://abims.sb-roscoff.fr) platform at the 
[Station Biologique de Roscoff](https://www.sb-roscoff.fr).  

    '''), className="bd-content-full")