import logging

from dash import Dash, html, dcc
import dash
import dash_bootstrap_components as dbc
import toml
import argparse

import phytobs_data_tools as pdt

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


def build_app(config):
    """
    Build the PHYTOBS Dash application.

    :param config: Configuration dictionary containing web application settings.
    :type config: dict

    :return: The constructed Dash application.
    """

    LOGGER.debug("Starting build_app")

    matomo_tracker_url = ''
    matomo_site_id = ''
    if 'matomo' in config and 'tracker_url' in config['matomo'] and 'site_id' in config['matomo']:
        LOGGER.debug(
            f"Initializing Matomo collector with tracker url {config['matomo']['tracker_url']} and site id {config['matomo']['site_id']}")
        matomo_tracker_url = config['matomo']['tracker_url']
        matomo_site_id = config['matomo']['site_id']
    else:
        LOGGER.debug(f"Matomo configuration missing or incomplete. Skipping.")

    app = Dash('phytobsdash',
               title="PHYTOBS SNO Data Viewer",
               external_stylesheets=[dbc.themes.YETI, dbc.icons.BOOTSTRAP],
               meta_tags=[
                   {"name": "viewport", "content": "width=device-width, initial-scale=1"},
                   {"property": "matomo-tracker-url", "content": matomo_tracker_url},
                   {"property": "matomo-site-id", "content": matomo_site_id},

               ],
               use_pages=True,
               **config['webapp']
               )
    app.layout = html.Div([
        html.Div(children=[
            html.H1("PHYTOBS SNO Data Viewer", className="h1 bd-title font-weight-bolder")
        ],
            className="bd-background"),
        html.Div(
            dbc.Nav(
                [
                    dbc.NavItem(
                        dbc.NavLink(
                            f"{page['name']}", href=page["relative_path"],
                        )
                    )
                    for page in dash.page_registry.values()
                ], pills=True, fill=True),
            className="bd-top-sections navbar navbar-dark bg-dark"
        ),
        html.A(
            html.Img(
                src='assets/images/ir-ilico_logo.png',
                className='bd-ilicologo'
            ),
            href='https://www.ir-ilico.fr/?PagePrincipale',
            target='_blank'
        ),
        html.A(
            html.Img(
                src='assets/images/phytobs-logo.png',
                className='bd-phytobslogo'
            ),
            href="https://www.benthobs.fr",
            target='_blank'
        ),
        dash.page_container
    ])

    LOGGER.debug("Done build_app")
    return app


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='PHYTOBS Dashboard'
    )
    parser.add_argument('config', type=open)

    args = parser.parse_args()

    config = toml.load(args.config)

    (pdt.analyst_df, pdt.phytobs_df, pdt.hydro_df) = pdt.load_data(config)
    pdt.compute_most_abundant_taxa()
    pdt.compute_most_abundant_taxon_group()
    pdt.compute_survey_info()
    app = build_app(config)
    app.run_server(**config['server'])
