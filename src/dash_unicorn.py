import toml

import phytobs_data_tools as pdt
import dash_app

config = toml.load('/config/phytobsdash_config.toml')
(pdt.analyst_df, pdt.phytobs_df, pdt.hydro_df) = pdt.load_data(config)
pdt.compute_most_abundant_taxa()
pdt.compute_most_abundant_taxon_group()
pdt.compute_survey_info()
application = dash_app.build_app(config)

server = application.server

