FROM python:3.10-alpine
RUN apk add --update --no-cache gcc g++ libc-dev
RUN pip install --upgrade pip
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
WORKDIR /app
COPY ./src /app/src
WORKDIR /app/src
CMD ["gunicorn","dash_unicorn:server","-b","0.0.0.0:8050" ]
