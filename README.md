# PHYTOBS Dashboard

![PHYTOBS Dashboard Example Screenshot](screenshot.png)

## Project Summary

The PHYTOBS Dashboard aims to provide an easy to use on-line data visualisation tool, for microphytoplankton data collected in
the framework of the PHYTOBS National Observation Service ([PHYTOBS SNO](https://www.phytobs.fr)), which is part of the [ILICO](https://www.ir-ilico.fr) French Research Infrastructure.

The reference dashboard is available at (https://data.phytobs.fr/dash)

## Overall Architecture

The PHYTOBS Dashboard is a multi-page Dash-based application written in Python, and relying on the Pandas library for data manipulation as well as extra Boostrap components for UI widgets.

The data itself matches the latest release of the PHYTOBS SNO comprehensive datasets. It is cached locally after initially loading it from the PHYTOBS SNO [official download area](https://data.phytobs.fr). 

## Installation

The project comes with all the necessary files to build and deploy the Web application in the form of a Docker container.

Running the container requires a TOML-formatted configuration file that needs to be mapped to `/config/pyhtobsdash_config.toml` inside the container. A sample [configuration file](config/phytobsdash_config.toml) is provided, as well as a [Docker compose](docker-compose.yml) file.


## Contributors

Initial development was carried out during the internship of Fatima Ezzahraa EL HOUJJAJI under the supervision of Mark HOEBEKE (March to July 2023).

Further developments have been made since by Mark HOEBEKE.

## Copyright & License

The PHYTOBS Dashboard is Copyright © 2023-2024 Station Biologique de Roscoff / CNRS - Sorbonne Université

This program is free software: you can redistribute it and/or modify it under the terms of the [GNU Affero General Public License v3.0 or later](https://spdx.org/licenses/AGPL-3.0-or-later.html)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
